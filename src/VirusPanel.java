import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class generates the game panel and its functions (e.g. adding viruses to the game).
 * @author Noah Alerm
 */
public class VirusPanel extends JPanel implements MouseListener {
    //ATTRIBUTES
    private List<Virus> viruses = new ArrayList<>();

    private List<Thread> threads = new ArrayList<>();

    //CONSTRUCTOR
    /**
     * VirusPanel Constructor
     */
    public VirusPanel() {
        addMouseListener(this);
    }


    //METHODS
    /**
     * This method is used to create the image of the virus.
     * @param g Graphics
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;

        for (Virus virus: viruses) {
            g2.drawImage(virus.paintVirus(), virus.getX(), virus.getY(), null);
        }

        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * This method is used to add 5-10 virus to the game panel.
     */
    public void addVirus() {
        //RANDOM NUM
        Random random = new Random();
        int numVirus = random.nextInt((10-5)+1)+5;

        //ADDITION
        for (int i = 0; i < numVirus; i++) {
            //RANDOM VIRUS ATTRIBUTES
            int virusX = random.nextInt(getWidth()+1);
            int virusY = random.nextInt(getHeight()+1);
            double virusDX = -2 + (2 + 2) * random.nextDouble();
            double virusDY = -2 + (2 + 2) * random.nextDouble();

            //ACCELERATION CONTROL
            if (virusDX == 0)
                virusDX = 1;
            if (virusDY == 0)
                virusDY = 1;

            Virus virus = new Virus(virusX, virusY, virusDX, virusDY);
            virus.setPanelBounds(this);

            //MOVEMENT START
            Thread virusThread = new Thread(virus);
            threads. add(virusThread);
            virusThread.start();

            viruses.add(virus);
        }
    }

    //MOUSE LISTENER METHODS
    /**
     * This method is used to control what happens when a virus is clicked.
     * @param e MouseEvent
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();

        int counter = 0;

        for (Virus virus: viruses) {
            if (x >= virus.getX() && x <= (virus.getX() + virus.getRadius()*2) && y >= virus.getY() && y <=
                    (virus.getY()) + virus.getRadius()*2) {
                virus.kill();
                threads.get(counter).stop();
            }

            counter++;
        }

        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
