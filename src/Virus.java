import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public class Virus implements Runnable {
    //ATTRIBUTES
    private int x, y;
    private double dx, dy;
    private double radius = 25;
    private Image image = new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("virus.jpg"))).getImage();
    private boolean isAlive = true;

    //PANEL
    private JPanel gamePanel;

    //CONSTRUCTOR
    /**
     * Virus Constructor
     * @param x Horizontal axis
     * @param y Vertical axis
     * @param dx Horizontal acceleration
     * @param dy Vertical acceleration
     */
    public Virus(int x, int y, double dx, double dy) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
    }

    //GETTERS
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public double getRadius() {
        return radius;
    }

    //RUN
    @Override
    public void run() {
        while (true) {
            moveVirus();
            try {
                Thread.sleep(4);
            } catch (Exception e) {
                e.printStackTrace();
            }
            gamePanel.repaint();
        }
    }

    //METHODS
    /**
     * This method is used to set the game panel in order to get its bounds.
     * @param panel Game panel
     */
    public void setPanelBounds(JPanel panel) {
        gamePanel = panel;
    }

    /**
     * This method is used to start the virus' movement.
     */
    public void moveVirus() {
        //MOVEMENT
        x += dx;
        y += dy;

        //BOUNDS
        Rectangle bounds = gamePanel.getBounds();
        double panelWidth = bounds.getWidth();
        double panelHeight = bounds.getHeight();

        //BOUNCE
        if (x + radius > panelWidth || x < 0)
            dx = -dx;
        if (y + radius > panelHeight || y < 0)
            dy = -dy;
    }

    /**
     * This method is used to create the image of the virus.
     * @return virus image
     */
    public Image paintVirus() {
        if (isAlive)
            return image;
        else
            return new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("deadVirus.jpg")))
                    .getImage();
    }

    /**
     * This method is used to set the virus as not alive.
     */
    public void kill() {
        isAlive = false;
    }
}
