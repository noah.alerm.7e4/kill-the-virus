import javax.swing.*;
import java.awt.*;

/**
 * This class creates the game's frame and adds both panels to it.
 * @author Noah Alerm
 */
public class KillTheVirusGUI extends JFrame {
    //ATTRIBUTES
    private final VirusPanel virusPanel = new VirusPanel();

    //CONSTRUCTOR
    /**
     * KillTheVirusGUI Constructor
     */
    public KillTheVirusGUI() {
        //FRAME
        setBounds(600, 300, 1000, 500);
        setTitle("Kill The Virus!");
        setBackground(Color.WHITE);
        virusPanel.setBackground(Color.white);

        //PANELS
        add(virusPanel, BorderLayout.CENTER);
        JPanel buttons = new JPanel();

        //Buttons
        JButton gameButton = new JButton("Pandemic");
        JButton endButton = new JButton("End");

        buttons.add(gameButton);
        buttons.add(endButton);

        gameButton.addActionListener(e -> virusPanel.addVirus());
        endButton.addActionListener(e -> System.exit(0));

        add(buttons, BorderLayout.SOUTH);
    }
}
