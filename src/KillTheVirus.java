import javax.swing.*;

/**
 * This class is used as a main, which means that it initializes the whole program.
 * @author Noah Alerm
 */
public class KillTheVirus {
    //MAIN
    public static void main(String[] args) {
        //FRAME
        JFrame game = new KillTheVirusGUI();
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setVisible(true);
    }
}
